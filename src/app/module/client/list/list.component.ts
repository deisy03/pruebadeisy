import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { IClient } from '../client';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.styl']
})
export class ListComponent implements OnInit {
  user: IClient[];

  constructor(private userService:ClientService) { }

  ngOnInit(): void {
    this.clientService.query()
    .suscribe(res=>{
      console.warn('datos lista', res)
      this.client=res;
    }, error=>{
      console.warn('error', error)
    });
  }
}
