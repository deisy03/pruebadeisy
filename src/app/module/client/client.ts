export interface IClient {
    idClient?:number;
    nameClient?:string;
    phoneClient?:string;
    adressClient?:string;
    mailClient?:string;
    documentNumber?:string;
}

export class Client implements IClient{
    idClient?:number;
    nameClient?:string;
    phoneClient?:string;
    adressClient?:string;
    mailClient?:string;
    documentNumber?:string;
}