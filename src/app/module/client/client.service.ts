import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators'
import { IClient } from './client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  public query(): Observable<IClient[]>{
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/client`)
    .pipe(map(res=>{
      return res;
    }));
  }

  public save(client:IClient) : Observable<IClient>{
    return this.http.post<IClient>(`${environment.END_POINT}/api/client` ,client)
    .pipe(map(res=>{
      return res;
    }));
  }
}
